const puppeteer = require('puppeteer');
const fs = require('fs');
const kafka = require('kafka-node');


const kafkaClient = new kafka.KafkaClient({ kafkaHost: 'localhost:29092' });
const producer = new kafka.Producer(kafkaClient);
const kafkaTopic = 'price-data';

// Function to send data to Kafka
const sendDataToKafka = (data) => {
    const payloads = [
        { topic: kafkaTopic, messages: JSON.stringify(data) }
    ];
    producer.send(payloads, (err, data) => {
        if (err) {
            console.error('Error sending data to Kafka', err);
        } else {
            console.log('Data sent to Kafka', data);
        }
    });
};

// Function to save data to a file
const saveDataToFile = (data, filename) => {
    fs.writeFileSync(filename, JSON.stringify(data, null, 2), 'utf-8');
    console.log(`Data saved to ${filename}`);
};

// Main function to scrape data
const scrapeData = async (url) => {
    const browser = await puppeteer.launch({
        headless: false
    });
    const page = await browser.newPage();


    await page.goto(url, { waitUntil: 'networkidle2' });

    // Select elements using the appropriate selector
    const productsListClass = '.productOffers-listItem';
    const elements = await page.$$(productsListClass);
    const data = [];

    let id=1;
    for (let i=0; i<elements.length; i++) {
        
        const productInfo = await elements[i].evaluate(el =>el.getAttribute('data-dl-click'));
        // product info validation
        if (!productInfo) {
            console.warn('Unable to parse product info');
            continue;
        }
        const parsedInfo = JSON.parse(productInfo)

        // shop name validation
        const shopName = parsedInfo.shop_name
        if (!shopName) {
            console.warn(`Invalid shop name at position ${i+1}`);
            continue;
        }

        // price validation
        const price = parsedInfo.products[0].price
        if (!price || price <= 0) {
            console.warn(`Invalid shop name at position ${i+1}`);
            continue;
        }


        data.push({ [id]: {
            price: price,
            shop_name: shopName,
            position: i+1
        }});
        id++;
    }

    await browser.close();
    return data;
};

// Execute the scraping and process data
const processUrl = async (url, outputType) => {
    try {
        const data = await scrapeData(url);

        if (outputType === 'kafka') {
            sendDataToKafka(data);
        } else if (outputType === 'file') {
            saveDataToFile(data, 'price-data.json');
        } else {
            console.error('Invalid output type specified');
        }
    } catch (error) {
        console.error('Error scraping data', error);
    } finally {
        kafkaClient.close();
    }
};

const url = 'https://www.idealo.de/preisvergleich/OffersOfProduct/201846460_-aspirin-plus-c-forte-800-mg-480-mg-brausetabletten-bayer.html';
const args = process.argv.slice(2); 
const outputType = args[0] || 'file'; 

processUrl(url, outputType);