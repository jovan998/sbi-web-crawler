# SBI Web Crawler

## Running the project
Install dependecies:
```
npm install
```
Run the project (storing in file):
```
npm start
```

Kafka setup:
```
docker-compose up -d 
```

Run the project (storing in kafka):
```
npm run kafka
```